
public class AutoConvertDetail {
	public static void main(String[] args) {
		
		//有多种类型的数据混合运算时
		//系统首先自动将所有数据转换成容量最大的那种数据类型，然后再进行计算。
		int n1 = 10; // ok
		// float d1 = n1 + 1.1; // 错误
		float d1 = n1 + 1.1f;
		double d2 = 10 + 1.1;
		System.out.println(d2);

		// 当我们把精度（容量）大的数据类型赋值给精度（容量）小的数据类型时，就会报错，反之就会进行自动类型转换。
		int a = 'c';
		System.out.println(a);

		// (byte,short)和char之间不会相互自动转换。
		// byte、short、char 可以计算，在计算时转化为int类型。
		byte b3 = 1;
		// char n2 = n3; // 错误
		char n2 = 'a';
		int n3 = b3 + n2;
		System.out.println(n3);

		// 自动提升原则：表达式结果的类型自动提升为操作数中最大的类型。
		int q1 = 10;
		double q2 = q1 + 1.1;
		double q3 = q1 + q2;
		System.out.println(q3);
	}
}