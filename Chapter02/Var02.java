
public class Var02{

	// 编写一个main方法
	public static void main(String[] args) {
		// 记录人的信息
		int age = 16;
		double score = 89.9;	
		char gender = '男';
		String name = "Hogg";
		// 输出
		System.out.println("人的信息如下：");
		System.out.println("姓名：\t" + name);
		System.out.println("年龄：\t" + age);
		System.out.println("性别：\t" + gender);
		System.out.println("成绩：\t" + score);
	}
}