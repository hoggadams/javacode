
public class IntDetail {
	public static void main(String[] args) {
		
		// Java的整型常量（具体值）默认为int型，声明long型常量须后加'或L'
		int a = 10;
		long b = 10l;

		// java程序中变量常声明为int型，除非不足以表示大数，才使用long
	}
}