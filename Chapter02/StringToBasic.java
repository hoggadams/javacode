
public class StringToBasic {
	
	public static void main(String[] args) {
		
		// basic --> string 
		int n1 = 100;
		float n2 = 1.1f;
		double n3 = 1.1;
		boolean n4 = true;
		String s1 = n1 + " ";
		String s2 = n2 + " ";
		String s3 = n3 + " ";
		String s4 = n4 + " ";

		System.out.println(s1);
		System.out.println(s2);
		System.out.println(s3);
		System.out.println(s4);
		
		// string --> basic
		int num1 = Integer.parseInt("123");
		float num2 = Float.parseFloat(".123f");
		double num3 = Double.parseDouble(".123");
		boolean num4 = Boolean.parseBoolean("true");

		System.out.println(num1);
		System.out.println(num2);
		System.out.println(num3);
		System.out.println(num4);
		
		// string --> char
		String q1 = "123";
		char q2 = q1.charAt(0);
		System.out.println(q2);
	}
}