
// class 表示 Hello 是一个类，public表示 Hello 是一个公共类
// Hello {...} 表示一个类的开始和结束
// main {...} 表示一个方法的开始和结束
public class Hello{

	// 编写一个main方法
	public static void main(String[] args) {
		// 在屏幕上打印输出“Hello_World”
		// ; 表示一个语句的结束
		System.out.println("Hello_World");
	}
}