// 演示转义字符的使用
public class ChangeChar {

	// 编写一个main方法 
	public static void main(String[] args) {
		// \t --> 一个制表位，实现对齐功能
		System.out.println("北京\t上海\t广东");

		// \n --> 换行符
		System.out.println("Hello\nWorld");

		// \\ --> 输出一个\
		System.out.println("\\");

		// \"、\' --> 输出一个"(')
		System.out.println("\"\'");

		// \r --> 一个回车
		System.out.println("Hello\rWorld");
	}
}