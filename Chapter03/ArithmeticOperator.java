
public class ArithmeticOperator {

	public static void main(String[] args) {
		
		// / 使用
		System.out.println(10 / 4); // 2 -> int / int = int
		System.out.println((double)(10 /4)); // 2.0 -> 10/2=2(double)=2.0
		System.out.println(10.0/4); // 2.5

		// % 取模
		System.out.println(10 % 3); // 1
		System.out.println(-10 % 3); // -1
		System.out.println(10 % -3); // 1

		// ++ 使用
		int i = 10;
		i++; // i=i+1
		System.out.println(i); // 11
		++i; // i=i+1
		System.out.println(i); // 12
		// 独立使用前++和后++相同
		int j = i++;
		System.out.println(j); // j=12，i=13
		j = ++i;
		System.out.println(j); // 14
		// 在赋值中使用，i++先赋值后自增；++i先自增后赋值
	}
}