
public class ArithmeticOperatorExercise02 {

	public static void main(String[] args) {
		
		// 假如还有 59 天放假，问：合 xx 个星期零 xx 天
		int day = 59;
		int weekNum = 59 / 7;
		int dayOther = 59 % 7;
		System.out.println("还有"+weekNum+"周零"+dayOther+"天放假");

		// 定义一个变量保存华氏温度，华氏温度转换摄氏温度的公式为:
		// 5/9*(华氏温度-100),请求出华氏温度对应的摄氏温度
		double huaShi = 1234.6;
		double sheShi = 5.0/9*(huaShi-100);
		System.out.println("华氏度为"+huaShi+"摄氏度为"+sheShi);
	}
}